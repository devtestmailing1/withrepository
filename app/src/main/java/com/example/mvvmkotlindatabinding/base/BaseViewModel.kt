package com.example.mvvmkotlindatabinding.base

import androidx.lifecycle.ViewModel
import com.example.mvvmkotlindatabinding.injection.component.DaggerViewModelInjector
import com.example.mvvmkotlindatabinding.injection.component.ViewModelInjector
import com.example.mvvmkotlindatabinding.injection.module.NetworkModule
import com.example.mvvmkotlindatabinding.injection.module.RepoModule
import com.example.mvvmkotlindatabinding.repository.PostRepository
import com.example.mvvmkotlindatabinding.ui.post.PostListViewModel

abstract class BaseViewModel:ViewModel() {

    private val injector : ViewModelInjector = DaggerViewModelInjector
        .builder()
        .repository(RepoModule())
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    private fun inject(){
        when(this){
            is PostListViewModel -> injector.inject(this)
        }

        when(this){
            is PostListViewModel -> injector.PostRepo()
        }
    }


}