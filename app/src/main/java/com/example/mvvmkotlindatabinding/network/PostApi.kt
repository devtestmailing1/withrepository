package com.example.mvvmkotlindatabinding.network

import com.example.mvvmkotlindatabinding.model.DataModel
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PostApi {

    @GET("forecast?")
    fun getListData(@Query("zip") zipCode: Int , @Query("q") city:String
    , @Query("appid") appId:String): Observable<Response<DataModel>>

}