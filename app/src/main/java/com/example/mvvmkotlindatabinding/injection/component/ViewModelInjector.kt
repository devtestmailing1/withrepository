package com.example.mvvmkotlindatabinding.injection.component

import com.example.mvvmkotlindatabinding.injection.module.NetworkModule
import com.example.mvvmkotlindatabinding.injection.module.RepoModule
import com.example.mvvmkotlindatabinding.repository.PostRepository
import com.example.mvvmkotlindatabinding.ui.post.PostListViewModel
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(NetworkModule::class),(RepoModule::class)])
interface ViewModelInjector {

    fun inject(postListViewModel: PostListViewModel)

    fun PostRepo():PostRepository

    @Component.Builder
    interface Builder{
        fun build():ViewModelInjector
        fun repository(repository: RepoModule):Builder
        fun networkModule(networkModule:NetworkModule):Builder
    }
}