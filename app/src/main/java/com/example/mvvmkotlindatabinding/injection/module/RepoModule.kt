package com.example.mvvmkotlindatabinding.injection.module

import com.example.mvvmkotlindatabinding.repository.PostRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepoModule {

    @Singleton
    @Provides
    fun PostRepo():PostRepository{
        return PostRepository()
    }
}