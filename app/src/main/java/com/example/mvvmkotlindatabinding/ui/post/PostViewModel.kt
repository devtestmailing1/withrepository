package com.example.mvvmkotlindatabinding.ui.post


import androidx.lifecycle.MutableLiveData
import com.example.mvvmkotlindatabinding.base.BaseViewModel
import com.example.mvvmkotlindatabinding.model.DataModel

class PostViewModel:BaseViewModel() {
    private val temp = MutableLiveData<String>()
    private val humidity = MutableLiveData<String>()
    private val pressure = MutableLiveData<String>()
    private val description = MutableLiveData<String>()
    private val date = MutableLiveData<String>()

    fun bind(post: DataModel.List){
        temp.value =  "Temperature: " + post.main.temp.toString()
        humidity.value = "Humidity: " + post.main.humidity.toString()
        pressure.value = "Pressure: " + post.main.pressure.toString()
        description.value = "Description: " + post.weather[0].description
        date.value = "Date: "+ post.dtTxt
    }

    fun getTemp():MutableLiveData<String>{
        return temp
    }

    fun getHumidity():MutableLiveData<String>{
        return humidity
    }

    fun getPressure():MutableLiveData<String>{
        return pressure
    }

    fun getDescription():MutableLiveData<String>{
        return description
    }

    fun getDate():MutableLiveData<String>{
        return date
    }



}