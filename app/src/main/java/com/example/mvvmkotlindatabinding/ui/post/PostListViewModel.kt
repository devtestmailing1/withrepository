package com.example.mvvmkotlindatabinding.ui.post


import android.view.View
import androidx.lifecycle.MutableLiveData
import com.example.mvvmkotlindatabinding.base.BaseViewModel
import com.example.mvvmkotlindatabinding.model.DataModel
import com.example.mvvmkotlindatabinding.network.PostApi
import com.example.mvvmkotlindatabinding.repository.PostRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class PostListViewModel():BaseViewModel() {
    @Inject
    lateinit var postApi: PostApi
    @Inject
    lateinit var postRepository:PostRepository

     var a:String? = "";

    var loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    var errorMessage:MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { fetchData() }
    val postListAdapter: PostListAdapter = PostListAdapter()
    var liveResult:MutableLiveData<DataModel> = MutableLiveData()
    private lateinit var subscription:Disposable
    private var disposable:CompositeDisposable

    init {
        fetchData()
        disposable = CompositeDisposable()
    }

    fun fetchData(){

        postRepository.loadPosts(postApi)
        errorMessage = postRepository.errorMessage
        loadingVisibility = postRepository.loadingVisibility
        liveResult = postRepository.liveResult
    }

    fun updateList(liveResult: MutableList<DataModel.List>){
        postListAdapter.updatePostList(liveResult)
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

}