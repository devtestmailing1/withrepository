package com.example.mvvmkotlindatabinding.repository

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.example.mvvmkotlindatabinding.R
import com.example.mvvmkotlindatabinding.model.DataModel
import com.example.mvvmkotlindatabinding.network.PostApi
import com.example.mvvmkotlindatabinding.ui.post.ResponseObserver
import dagger.Module
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

@Module
class PostRepository {

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage:MutableLiveData<Int> = MutableLiveData()
    val liveResult:MutableLiveData<DataModel> = MutableLiveData()


    public fun loadPosts(postApi: PostApi){

        postApi.getListData(110001,"Delhi","8d9aab5a760f1d93be24e2385cea3f33").subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe{onRetrievePostListStart()}
            .doOnTerminate{onRetrievePostListFinish()}
            .subscribe(object : ResponseObserver<DataModel>() {
                override fun onNetworkError(e: Throwable?) {
                    Log.e("resulttss", "dfdf")
                    onRetrievePostListError()
                }
                override fun onServerError(e: Throwable?, code: Int) {
                    Log.e("resulttss", "dfdfdfdd")
                    onRetrievePostListError()
                }
                override fun onNext(t: Response<DataModel>) {
                    Log.e("resulttss", "fdfdf333")
                    t.body()?.let { onRetrievePostListSuccess(it) }
                }
                override fun onNoData() {
                    Log.e("resulttss","ddd5555")
                }
            })


    }



    private fun onRetrievePostListStart(){
        loadingVisibility.value= View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePostListFinish(){
        loadingVisibility.value = View.GONE

    }

    private fun onRetrievePostListSuccess(mutableList: DataModel) {
        Log.e("resulttss", mutableList.list.toString())
        liveResult.value = mutableList

    }

    private fun onRetrievePostListError(){
        errorMessage.value = R.string.post_error
    }

}