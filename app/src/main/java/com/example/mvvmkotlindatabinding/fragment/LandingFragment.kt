package com.example.mvvmkotlindatabinding.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.mvvmkotlindatabinding.R
import com.example.mvvmkotlindatabinding.databinding.HomeFragmentBinding
import com.example.mvvmkotlindatabinding.injection.ViewModelFactory
import com.example.mvvmkotlindatabinding.ui.post.PostListViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.home_fragment.*

class LandingFragment : Fragment() {

    private lateinit var viewModel: PostListViewModel
    private var errorSnackbar: Snackbar? = null
    private lateinit var binding: HomeFragmentBinding
    lateinit var mSwipeRefreshLayout: SwipeRefreshLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.landing_fragment, container, false)
        return view
    }



}