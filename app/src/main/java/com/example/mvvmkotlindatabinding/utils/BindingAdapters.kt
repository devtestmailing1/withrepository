package com.example.mvvmkotlindatabinding.utils

import android.content.SharedPreferences
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmkotlindatabinding.model.DataModel
import com.example.mvvmkotlindatabinding.ui.post.MainActivity
import com.example.mvvmkotlindatabinding.utils.extension.getParentActivity


@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View, visibility: MutableLiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && visibility != null) {
        visibility.observe(parentActivity, Observer { value ->
            view.visibility = value ?: View.VISIBLE
            Log.e("observervalue>", value.toString())
        })
    }
}

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && text != null) {
        text.observe(parentActivity, Observer { value -> view.text = value ?: "" })
    }
}

@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    view.adapter = adapter
}


/*@BindingAdapter("android:onClick")
fun setOnClick(view: LinearLayout){

    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null) {

    }

    view.setOnClickListener(View.OnClickListener {

        val sharedPref: SharedPreferences = parentActivity?.applicationContext!!.getSharedPreferences("zip")

        ( view.getParentActivity() as MainActivity).GoToNextFragment()

    })}*/
